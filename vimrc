set nocompatible              " be iMproved, required

" Enable completion where available.
" This setting must be set before ALE is loaded.
"
" You should not turn this setting on if you wish to use ALE as a completion
" source for other completion plugins, like Deoplete.
let g:ale_completion_enabled = 0
set omnifunc=ale#completion#OmniFunc

" set the runtime path to include Vundle and initialize
call plug#begin('~/.vim/plugged')
" let Vundle manage Vundle, required
Plug 'ntpeters/vim-better-whitespace'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
"lug 'godlygeek/tabular'
"lug 'JamshedVesuna/vim-markdown-preview'
Plug 'vim-python/python-syntax'
Plug 'davidhalter/jedi-vim'
Plug 'christianrondeau/vim-base64'
"lug 'tpope/vim-unimpaired'
"lug 'pangloss/vim-javascript'    " JavaScript support
"lug 'leafgarland/typescript-vim' " TypeScript syntax
"lug 'maxmellon/vim-jsx-pretty'   " JS and JSX syntax
"lug 'jparise/vim-graphql'        " GraphQL syntax
Plug 'neoclide/coc.nvim' , { 'branch' : 'release' } " Language server stuff
"Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
"Plug 'junegunn/fzf.vim'
"Plug 'Aarleks/zettel.vim' " Notes
"Plug 'madox2/vim-ai', { 'do': './install.sh' } " chatgpt
call plug#end()
filetype plugin indent on    " required
autocmd BufRead,BufNewFile *.py filetype indent off
" This will autocomplete on every . which became a pain quickly(Especially if
" you paste code
" au filetype go inoremap <buffer> . .<C-x><C-o>

" Related to JamshedVesuna/vim-markdown-preview
" to set markdown to be github flavored(requires grip to be installed)
"let vim_markdown_preview_github=1
"let vim_markdown_preview_browser='Google Chrome'

let mapleader="," " Set comma as map leader

" python syntax highlighting
let g:python_highlight_all = 1

" vim-better-whitespace
let g:strip_whitespace_on_save = 1
let g:strip_only_modified_lines=0

" Go-vim
" On save, run lint and put output into quick window
"let g:go_metalinter_autosave = 1
let g:go_fmt_command = "gofmt"
" let g:go_fmt_options = "-s"
" add what type a variable is in navbar
let g:go_auto_type_info = 1
let g:go_test_timeout = 60

" vimp-markdown-preview use grip
" brew install grip
let vim_markdown_preview_github=1

" put buffer into background so you don't have to save it
set hidden

"""""""" Mapps """"""""
nmap <leader>r :!%<CR>
map <silent> <leader>M :%s/<C-V><C-M>//<CR>
map <leader>dt :!python -m doctest %<CR>
map <leader>s  :setlocal spell spelllang=en_us<CR>
map <leader>c  :!cat % \| pbcopy<CR>
map <leader>ni :setl noai nocin nosi inde= <CR>
map <leader>jj :%!python -mjson.tool<CR>
map <leader>gt :!go test ./...<CR>
map <leader>gtv :!go test -v ./...<CR>
map <leader>gtt :GoTest<CR>
map <leader>gb :GoBuild<CR>
map <leader>gi :GoImports<CR>
map <leader>gdef :GoDef<CR>
map <leader>gref :GoDef<CR>

" maps for chatgpt
nnoremap <leader>a :AI<CR>
vnoremap <leader>a :AI<CR>

nnoremap ; : " Use ; instead of : Nobody uses ; anyways
noremap <C-i> :exe '! '.@0 <CR>
" Ctrl+e/y scrolls viewport
nnoremap <C-e> 2<C-e>
nnoremap <C-y> 2<C-y>
" Remap j and k to act as expected when used on long, wrapped, lines
nnoremap j gj
nnoremap k gk
" Easy window navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
nnoremap <leader>w <C-w>v<C-w>l
" F8 to turn off indent
nnoremap <F8> :setl noai nocin nosi inde=<CR>
" Complete whole filenames/lines with a quicker shortcut key in insert mode
imap <C-f> <C-x><C-f>
imap <C-l> <C-x><C-l>

"""""" Misc stuff """"""""
"set relativenumber  " Line numbers are relative up and down to cursor
"set linebreak       " Breaks lines longer than textwidth (requires wraplines)
"et showbreak=+++   " what to show on wrapped lines
"et textwidth=80    " How many characters to wrap at

set ffs=unix
syntax on " Syntax highlighting
set background=dark " Dark backgrounds
set showmatch " set show matching parenthesis
set ignorecase " ignore case when searching
set smartcase " ignore case if search pattern is all lowercase,
set scrolloff=10 " keep 10 lines off the edges of the screen when scrolling
set virtualedit=all " allow the cursor to go in to "invalid" places
"set listchars=tab:▸\ ,trail:·,extends:#,nbsp:· " Put characters in for normally transparent characters(space, tab...)
set incsearch " show search matches as you type
set iskeyword-=_ " allow word jumping to treat underscore as a separator
set laststatus=2 " tell VIM to always put a status line in, even
 "    if there is only one window
set history=1000 " remember more commands and search history
set undolevels=1000 " use many muchos levels of undo
set nomodeline " disable mode lines (security measure)
"set cursorline " underline the current line, for quick orientation
" Should send to system clipboard(but I think then not to vim register :( )
" set clipboard=unnamed

" TABS
set ts=2 " A tab is 4 spaces
set sw=2 " Backspace deletes spaces as if a tab was there
set expandtab " Expand tabs by default

" Fixes annoying stuff
set hls! " Turn off search highlighting?
set nohlsearch " Turn off seach highlighting?
set backspace=indent,eol,start " allow backspacing over everything in insert mode
nohl " Turn off highlighting everything
set fileformats="unix,dos,mac"
set formatoptions+=1 " When wrapping paragraphs, don't end lines
set nobackup " do not keep backup files, it's 70's style cluttering
set noswapfile " do not write annoying intermediate swap files,
 "    who did ever restore from swap files anyway?
set directory=~/.vim/.tmp,~/tmp,/tmp
 " store swap files in one of these directories
 "    (in case swapfile is ever turned on)
set wildignore=*.swp,*.bak,*.pyc,*.class
set splitbelow
set splitright

"""""""
" Stuff I'm not sure about from William
"set shiftwidth=4 " number of spaces to use for autoindenting
"set shiftround " use multiple of shiftwidth when indenting with '<' and '>'
"set smarttab " insert tabs on the start of a line according to
"set gdefault " search/replace "globally" (on a line) by default

""" Not sure exactly what this all does -- Experimental
" Highlighting {{{
if &t_Co >= 256 || has("gui_running")
 colorscheme molokai
endif

if exists('+colorcolumn')
  set colorcolumn=160
else
  au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>160v.\+', -1)
endif

" What does this little gem do?
if version >= 703
    nmap <leader>r :set relativenumber!<CR>
    set undofile                " keep a persistent backup file
    set undodir=~/.vim/.undo,~/tmp,/tmp
endif

autocmd FileType make set noexpandtab shiftwidth=8 softtabstop=0
autocmd FileType ruby set expandtab shiftwidth=2 softtabstop=2 tabstop=2
autocmd FileType yaml set expandtab shiftwidth=2 softtabstop=2 tabstop=2
autocmd FileType json set expandtab shiftwidth=2 softtabstop=2 tabstop=2
autocmd FileType python set expandtab shiftwidth=4 softtabstop=4 tabstop=4
autocmd FileType groovy set expandtab shiftwidth=2 softtabstop=2 tabstop=2
autocmd FileType java set expandtab shiftwidth=4 softtabstop=4 tabstop=4
autocmd FileType go set noexpandtab shiftwidth=4 softtabstop=4 tabstop=4
autocmd FileType markdown set noexpandtab shiftwidth=2 softtabstop=2 tabstop=2

cabbr <expr> %% expand('%:p:h')

"let vim_markdown_preview_hotkey='<C-S-m>'

" Toggle nopaste on/off
" The first line sets a mapping so that pressing F2 in normal mode will invert
" the 'paste' option, and will then show the value of that option. The second
" line allows you to press F2 when in insert mode, to toggle 'paste' on and
" off. The third line enables displaying whether 'paste' is turned on in
" insert mode.
nnoremap <F5> :set invpaste paste?<CR>
set pastetoggle=<F5>
set showmode

" Coc Setup
" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)
