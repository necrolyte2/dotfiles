#
# ~/.bashrc
#

if [ -f /etc/bashrc ]
then
    . /etc/bashrc
fi

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

if [ -e /etc/bash_completion.d/git-prompt ]
then
    . /etc/bash_completion.d/git-prompt
    GIT_PS1_SHOWDIRTYSTATE=1
    PS1='[\w]$(__git_ps1)\$ '
elif [ -e /etc/bash_completion.d/git ]
then
    . /etc/bash_completion.d/git
    GIT_PS1_SHOWDIRTYSTATE=1
    PS1='[\w$(__git_ps1 " (%s)")]\$ '
else
    PS1='[\w]($KUBECTL_CONTEXT:$KUBECTL_NAMESPACE)\$ '
fi

##### Essential Environment Variables #####
export TERM=screen-256color
# Default Editor
EDITOR="/usr/bin/vim"

# Allow for more customization for each machine
if [ -e ~/.bashrc.d ]
then
    for f in ~/.bashrc.d/*
    do
        . ${f}
    done
fi

# Directory Colors
# man dir_colors for more information on this
LS_COLORS='rs=0:di=1;33:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32'
export LS_COLORS

# Set shell mode to vi keybindings
set -o vi

# erasedups means duplicate commands in a row only show as one
# in your history
export HISTCONTROL=erasedups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000

# https://github.com/dylanaraps/pure-bash-bible#get-the-current-cursor-position
# new_line_ps1() {
#   local _ y x _
#   local LIGHT_YELLOW="\001\033[1;93m\002"
#   local     RESET="\001\e[0m\002"
#
#   IFS='[;' read -p $'\e[6n' -d R -rs _ y x _
#   if [[ "$x" != 1 ]]; then
#     printf "\n${RESET}"
#   fi
# }
#PS1="$PS1\$(new_line_ps1)"
