#!/bin/bash

# A not so perfect installer that can be refactored over time
# For now does its job of getting from 0 -> done in less than 10 minutes

function install_linux() {
	echo "Linux installer"
	ssh-keygen -f ~/.ssh/id_rsa -b 4096
	cat ~/.ssh/id_rsa.pub | xclip -selection clipboard
	read -p "Create new key and add the one that was just copied into clipboard"
	open https://gitlab.com/-/user_settings/ssh_keys/
	read -p "continue?"
	sudo apt-get update
	sudo apt-get install -y git tmux wget curl xclip rcm thefuck git make unzip gcc golang gopls ripgrep
	install_nvim
	clone_dotfiles
	rcup
	if [[ ! -e /tmp/miniconda.sh ]]; then
		curl https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh > /tmp/miniconda.sh
	fi
	install_miniconda
}

function install_mac() {
	echo "Mac installer"
	ssh-keygen -f ~/.ssh/id_rsa -b 4096
	cat ~/.ssh/id_rsa.pub | pbcopy
	read -p "Create new key and add the one that was just copied into clipboard"
	open https://gitlab.com/-/user_settings/ssh_keys/
	read -p "continue?"
	brew install git tmux wget curl rcm
	install_nvim
	clone_dotfiles
	rcup
	if [[ ! -e /tmp/miniconda.sh ]]; then
		curl https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh > /tmp/miniconda.sh
	fi
	install_miniconda
}

function install_miniconda() {
	if [[ ! -e ~/miniconda3 ]]
	then
		bash /tmp/miniconda.sh -b -f
		. ~/miniconda3/bin/activate
	fi
}

function install_nvim() {
	sudo rm -rf /opt/nvim*
	curl -Ls https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz | sudo tar -C /opt -xzvf -
}

function clone_dotfiles() {
   git clone git@gitlab.com:necrolyte2/dotfiles.git ~/.dotfiles
   cd ~/.dotfiles
}

if apt-get --help >/dev/null 2>&1
then
	install_linux
else
	install_mac
fi
