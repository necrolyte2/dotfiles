# Dotfiles

My home directory goodies

# Setup

```
curl 'https://gitlab.com/necrolyte2/dotfiles/-/raw/master/install.sh?ref_type=heads&inline=false' > install.sh
bash install.sh
```

# Adding new files to repo

## Files specific to a computer

```
mkrc -b $(hostname) .mydotfile
```

## Files specific to some environment(tag)

```
mkrc -t <tag> .mydotfile
```

## General files applicable to all

```
mkrc .mydotfile
```
